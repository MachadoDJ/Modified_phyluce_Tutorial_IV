#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Requires Python v2.7+ (it should work on Python 3.5+ as well) with BioPython

# Name: modify_headers_4_phyluce.py
# Author: Denis Jacob Machado (machadodj<at>usp.br)

from Bio import SeqIO
import argparse, re, sys

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--index_file", type=str, help="Path to tab-separated file with the path to the original multifasta file on the first column and path to the modified multifasta file to be written on the second column")
parser.parse_args()

def main():
	handle = open(args.input, "r")
	for line in handle.readlines():
		line = re.sub("#.+","",line.strip())
		if (line):
			try:
				original, modified = line.split("\t")
			except:
				pass
			else:
				modify_multifasta(original, modified)
				sys.stdout.write("In: {}; Out: {}\n".format(original, modified))
	handle.close()
	return

def modify_multifasta(original, modified):
	with open(original, "r") as infile:
		with open(modified, "w") as outf:
			for seq in SeqIO.parse(infile, 'fasta'):
				seq.name = ""
				seq.description = ""
				outf.write(seq.format('fasta'))
	return

main()

exit()
