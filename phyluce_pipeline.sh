#!/bin/bash
set -o errexit

## NOTES
# Tutorial available at http://phyluce.readthedocs.io/en/latest/tutorial-four.html#
# From data download to the creation of a master bait list

## DEPENDENCIES
# Anaconda/ Miniconda
# Phyluce
# SAMtools (should become available after Phyluce installation with CONDA)
# BEDtools (should become available after Phyluce installation with CONDA)
# mod_headers.py (homemade Python script)
# ART (art_illumina v2016.06.05)
# BLAT (Src35)
# BLAT (Src35)
# Stampy
# phyluce_probe_slice_sequence_from_genomes (modified from original, lines 267–273, skip questions)

## DEFINE FUNCTIONS

function download {
	# Anoplophora glabripennis (Asian longhorned beetle)
	wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/390/285/GCA_000390285.1_Agla_1.0/GCA_000390285.1_Agla_1.0_genomic.fna.gz
	# Agrilus planipennis (emerald ash borer)
	wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/699/045/GCA_000699045.1_Apla_1.0/GCA_000699045.1_Apla_1.0_genomic.fna.gz
	# Leptinotarsa decemlineata (Colorado potato beetle)
	wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/500/325/GCA_000500325.1_Ldec_1.5/GCA_000500325.1_Ldec_1.5_genomic.fna.gz
	# Onthophagus taurus (beetles)
	wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/648/695/GCA_000648695.1_Otau_1.0/GCA_000648695.1_Otau_1.0_genomic.fna.gz
	# Dendroctonus ponderosae (mountain pine beetle)
	wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/355/655/GCA_000355655.1_DendPond_male_1.0/GCA_000355655.1_DendPond_male_1.0_genomic.fna.gz
	# Tribolium castaneum (red flour beetle)
	wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/002/335/GCA_000002335.2_Tcas_3.0/GCA_000002335.2_Tcas_3.0_genomic.fna.gz
	# Mengenilla moldrzyki (twisted-wing parasites) [Outgroup]
	wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/281/935/GCA_000281935.1_Memo_1.0/GCA_000281935.1_Memo_1.0_genomic.fna.gz
} # CHECKED

function extract_sequences {
	for f in *.gz ; do
		gunzip ${f}
		wait
	done
} # CHECKED

function mod_headers {
	python modify_headers_4_phyluce.py -i example_filename_table.tsv
	wait
} # CHECKED

function remove_redundant_ncbi {
	rm *.fna
	wait
} # CHECKED

function organize_directories {
	mkdir genomes
	for file in *.fasta ; do
		mkdir genomes/${file%.*}
		mv $file genomes/${file%.*}/
		wait
	done
} # CHECKED

function convert_2bit {
	cd ${PATH_TO_WD}/genomes
	for file in * ; do
		faToTwoBit $file/$file.fasta $file/${file%.*}.2bit
	done
} # CHECKED

function simulate_reads_using_art {
	cd ${PATH_TO_WD}
	mkdir reads
	cd reads
	art_illumina --paired --in ${PATH_TO_WD}/genomes/agrPla1/agrPla1.fasta \
		--out agrPla1-pe100-reads --len 100 --fcov 2 --mflen 200 --sdev 150 \
		-ir 0.0 -ir2 0.0 -dr 0.0 -dr2 0.0 -qs 100 -qs2 100 -na &
	art_illumina --paired --in ${PATH_TO_WD}/genomes/anoGla1/anoGla1.fasta \
		--out anoGla1-pe100-reads --len 100 --fcov 2 --mflen 200 --sdev 150 \
		-ir 0.0 -ir2 0.0 -dr 0.0 -dr2 0.0 -qs 100 -qs2 100 -na &
	art_illumina --paired --in ${PATH_TO_WD}/genomes/denPon1/denPon1.fasta \
		--out denPon1-pe100-reads --len 100 --fcov 2 --mflen 200 --sdev 150 \
		-ir 0.0 -ir2 0.0 -dr 0.0 -dr2 0.0 -qs 100 -qs2 100 -na &
	art_illumina --paired --in ${PATH_TO_WD}/genomes/lepDec1/lepDec1.fasta \
		--out lepDec1-pe100-reads --len 100 --fcov 2 --mflen 200 --sdev 150 \
		-ir 0.0 -ir2 0.0 -dr 0.0 -dr2 0.0 -qs 100 -qs2 100 -na &
	art_illumina --paired --in ${PATH_TO_WD}/genomes/ontTau1/ontTau1.fasta \
		--out ontTau1-pe100-reads --len 100 --fcov 2 --mflen 200 --sdev 150 \
		-ir 0.0 -ir2 0.0 -dr 0.0 -dr2 0.0 -qs 100 -qs2 100 -na
	wait
	# NOTE: we’ve dropped menMol1 from the read simulation process. This is largely
	# because it is an outgroup to beetles. We’ll use it later, when we’re performing in
	# silico tests of the UCE bait set.
} # CHECKED

function merge_read_info { # Modified from the original
	cd ${PATH_TO_WD}/reads
	for critter in agrPla1 anoGla1 denPon1 lepDec1 ontTau1 ; do
		printf "...working on ${critter}\n"
		cat  ${critter}-pe100-reads1.fq ${critter}-pe100-reads2.fq >  ${critter}-pe100-reads.fq
		rm   ${critter}-pe100-reads1.fq ${critter}-pe100-reads2.fq
		gzip ${critter}-pe100-reads.fq
	done
} # CHECKED

function prepare_base_genome {
	cd ${PATH_TO_WD}
	ln -s ${PATH_TO_WD}/genomes/triCas1/triCas1.fasta .
	mkdir ${PATH_TO_WD}/base
	cd    ${PATH_TO_WD}/base
	cp    ${PATH_TO_WD}/genomes/triCas1/triCas1.fasta ./ # Correction: location of triCas1.fasta
	wait
	stampy.py --species="tribolium-castaneum" --assembly="triCas1" -G triCas1 triCas1.fasta
	wait
	stampy.py -g triCas1 -H triCas1
	wait
} # CHECKED

function align_reads_2_genome {
	mkdir ${PATH_TO_WD}/alignments
	cd    ${PATH_TO_WD}
	export cores=24
	export base=triCas1
	export base_dir=${PATH_TO_WD}/alignments
	for critter in agrPla1 anoGla1 denPon1 lepDec1 ontTau1 ; do
		export reads=${critter}-pe100-reads.fq.gz
		mkdir -p ${base_dir}/${critter}
		cd ${base_dir}/${critter}
		stampy.py --maxbasequal 93 -g ${PATH_TO_WD}/base/${base} \
			-h ${PATH_TO_WD}/base/${base} --substitutionrate=0.05 -t${cores} \
			--insertsize=400 \
			-M ${PATH_TO_WD}/reads/${reads} | samtools view -Sb - > ${critter}-to-${base}.bam
		wait
	done
	cd ${PATH_TO_WD}/alignments
	mkdir all
	for critter in agrPla1 anoGla1 denPon1 lepDec1 ontTau1 ; do
		samtools view -h -F 4 -b ${critter}/${critter}-to-triCas1.bam > ${critter}/${critter}-to-triCas1-MAPPING.bam
		wait
		# rm $critter/$critter-to-triCas1.bam
		ln -s ../${critter}/${critter}-to-triCas1-MAPPING.bam all/${critter}-to-triCas1-MAPPING.bam
		wait
	done
} # CHECKED

function convert_bams_2_beds {
	cd ${PATH_TO_WD}
	mkdir bed
	cd ${PATH_TO_WD}/bed
	for i in ../alignments/all/*.bam ; do
		echo ${i}
		bedtools bamtobed -i ${i} -bed12 > `basename ${i}`.bed
	done
} # CHECKED

function sort_beds {
	cd ${PATH_TO_WD}/bed
	for i in *.bed ; do
		printf "${i}\n"
		bedtools sort -i ${i} > ${i%.*}.sort.bed
	done
} # CHECKED

function merge_overlapping_intervals {
	cd ${PATH_TO_WD}/bed
	for i in *.bam.sort.bed ; do
		printf "${i}\n"
		bedtools merge -i ${i} > ${i%.*}.merge.bed
		wait
	done
	for i in *.bam.sort.merge.bed ; do
		printf "`wc -l ${i}`\n"
	done
} # CHECKED

function remove_repetitive_intervals {
	cd ${PATH_TO_WD}/bed
	for i in *.sort.merge.bed ; do
		phyluce_probe_strip_masked_loci_from_set --bed ${i} \
			--twobit ../genomes/triCas1/triCas1.2bit \
			--output ${i%.*}.strip.bed --filter-mask 0.25 --min-length 80
	done;
} # CHECKED

function create_bed_config_file {
	cd ${PATH_TO_WD}/bed
	printf "[beds]\n" > bed-files.conf
	for f in *.strip.bed ; do printf "`expr "${f}" : '^\(.[a-zA-Z0-9]*\)'`:${f}\n" >> bed-files.conf ; done
} # CHECKED

function find_locus_in_multiple_genomes {
	cd ${PATH_TO_WD}/bed
	phyluce_probe_get_multi_merge_table --conf bed-files.conf \
		--base-taxon triCas1 --output coleoptera-to-triCas1.sqlite
	wait
} # CHECKED

function find_shared_conserved_loci {
	cd ${PATH_TO_WD}/bed
	phyluce_probe_query_multi_merge_table --db coleoptera-to-triCas1.sqlite --base-taxon triCas1
	wait
	# Basically, the question boils down to “Should I select only the set of loci shared
	# by all exemplars and the base genome or shoul I be more liberal?”. It’s also a hard
	# question to answer. In most cases, I’m pretty happy selecting n-1 or n-2 where n is
	# the total number of exemplar taxa. In the example below, however, we’ve selected n
	# as the “ideal”. This is largely because we have so little information about
	# coleopteran genomes - so we want to be pretty darn sure these loci are found in
	# most/all of them.
	phyluce_probe_query_multi_merge_table --db coleoptera-to-triCas1.sqlite \
		--base-taxon triCas1 --output triCas1+5.bed --specific-counts 5
} # CHECKED

function extract_fasta_from_base {
	cd ${PATH_TO_WD}/bed
	phyluce_probe_get_genome_sequences_from_bed --bed triCas1+5.bed \
		--twobit ../genomes/triCas1/triCas1.2bit --buffer-to 160 --output triCas1+5.fasta
	wait
} # CHECKED

function design_temporary_bait {
	cd ${PATH_TO_WD}/bed
	phyluce_probe_get_tiled_probes \
	--input triCas1+5.fasta \
	--probe-prefix "uce-" \
	--design coleoptera-v1 \
	--designer faircloth \
	--tiling-density 3 \
	--two-probes \
	--overlap middle \
	--masking 0.25 \
	--remove-gc \
	--output triCas1+5.temp.probes
} # CHECKED

function remove_duplicates_from_temporary_bait {
	cd ${PATH_TO_WD}/bed
	phyluce_probe_easy_lastz \
	--target triCas1+5.temp.probes --query triCas1+5.temp.probes \
	--identity 50 --coverage 50 \
	--output triCas1+5.temp.probes-TO-SELF-PROBES.lastz
	wait
	phyluce_probe_remove_duplicate_hits_from_probes_using_lastz \
	--fasta triCas1+5.temp.probes \
	--lastz triCas1+5.temp.probes-TO-SELF-PROBES.lastz \
	--probe-prefix=uce-
	wait
} # CHECKED

function align_baits_against_exemplar_genomes {
	cd ${PATH_TO_WD}
	mkdir probe-design
	cd ${PATH_TO_WD}/probe-design # Modified from original - set the correct working directory
	mkdir coleoptera-genome-lastz # Modified from original: Create an empty directory required for the next step
	phyluce_probe_run_multiple_lastzs_sqlite \
		--probefile ${PATH_TO_WD}/bed/triCas1+5.temp-DUPE-SCREENED.probes \
		--scaffoldlist agrPla1 anoGla1 denPon1 lepDec1 ontTau1 triCas1 menMol1 \
		--genome-base-path ${PATH_TO_WD}/genomes \
		--identity 50 \
		--cores 24 \
		--db triCas1+5+menMol1.sqlite \
		--output coleoptera-genome-lastz
	wait
	# This next few ines were added to the orignal pipeline to automatically create the
	# required configuration file
	printf "[scaffolds]\n" > coleoptera-genome.conf
	for f in ${PATH_TO_WD}/genomes/*/*.2bit ; do printf "`echo ${f%.2bit} | grep -oE '[^/]+$'`:${f}\n" >> coleoptera-genome.conf ; done
	# Here buffering each locus to 180 bp to give us a little more room to work with
	# during the probe design step.
	phyluce_probe_slice_sequence_from_genomes \
		--conf coleoptera-genome.conf \
		--lastz coleoptera-genome-lastz \
		--probes 180 \
		--name-pattern "triCas1+5.temp-DUPE-SCREENED.probes_v_{}.lastz.clean" \
		--output coleoptera-genome-fasta
	# You might encounter a problem here, since phyluce_probe_slice_sequence_from_genomes
	# might stop and ask you about creating/ overwriting the coleoptera-genome-fasta
	# directory (it seems it asks you if you want to overwrite it even if there was
	# originally no directory there!). I edited phyluce_probe_slice_sequence_from_genomes
	# to avoid this problem.
	wait
} # CHECKED

function find_consistent_loci {
	cd ${PATH_TO_WD}/probe-design
	phyluce_probe_get_multi_fasta_table \
		--fastas coleoptera-genome-fasta \
		--output multifastas.sqlite \
		--base-taxon triCas1
	wait
	phyluce_probe_query_multi_fasta_table \
		--db multifastas.sqlite \
		--base-taxon triCas1
	wait
	phyluce_probe_query_multi_fasta_table \
		--db multifastas.sqlite \
		--base-taxon triCas1 \
		--output triCas1+5-back-to-4.conf \
		--specific-counts 4
	wait
	phyluce_probe_query_multi_fasta_table \
		--db multifastas.sqlite \
		--base-taxon triCas1
	wait
	phyluce_probe_query_multi_fasta_table \
		--db multifastas.sqlite \
		--base-taxon triCas1 \
		--output triCas1+5-back-to-4.conf \
		--specific-counts 4
	wait
} # CHECKED

function design_baits_using_all_genomes {
	cd ${PATH_TO_WD}/probe-design
	phyluce_probe_get_tiled_probe_from_multiple_inputs \
		--fastas ${PATH_TO_WD}/probe-design/coleoptera-genome-fasta \
		--multi-fasta-output triCas1+5-back-to-4.conf \
		--probe-prefix "uce-" \
		--designer faircloth \
		--design coleoptera-v1 \
		--tiling-density 3 \
		--overlap middle \
		--masking 0.25 \
		--remove-gc \
		--two-probes \
		--output coleoptera-v1-master-probe-list.fasta
	wait
	# Note that the number of baits that we’ve designed to target ~1200 conserved loci is
	# quite high - this is because we’re including roughly 2 baits for ~1200 loci across 7
	# exemplar taxa (16926 is the theoretical maximum).
} # CHECKED

function remove_duplicates_from_bait_set {
	cd ${PATH_TO_WD}/probe-design
	phyluce_probe_easy_lastz \
		--target coleoptera-v1-master-probe-list.fasta \
		--query coleoptera-v1-master-probe-list.fasta \
		--identity 50 \
		--coverage 50 \
		--output coleoptera-v1-master-probe-list-TO-SELF-PROBES.lastz
	wait
	phyluce_probe_remove_duplicate_hits_from_probes_using_lastz \
		--fasta coleoptera-v1-master-probe-list.fasta \
		--lastz coleoptera-v1-master-probe-list-TO-SELF-PROBES.lastz \
		--probe-prefix=uce-
	wait
# The master probe list that has been filtered of putatively duplicate loci is now located
# in coleoptera-v1-master-probe-list-DUPE-SCREENED.fasta.
} # CHECKED

function subsetting_master_probe_list {
# Sometimes we might not want to synthesize all of the baits for all of the loci. For
# instance, we might be enriching loci from species that are nested within the clade
# defined by ((‘Anoplophora glabripennis (Asian longhorned beetle)’:’Leptinotarsa
# decemlineata (Colorado potato beetle)’)’Dendroctonus ponderosae (mountain pine
# beetle)’), and because we’re only working with these species, we might want to drop the
# baits targeting UCE loci in Agrilus planipennis (emerald ash borer), Tribolium castaneum
# (red flour beetle), Onthophagus taurus (taurus scarab), and Mengenilla moldrzyki
# (Strepsiptera). This is actually pretty easy to do - we just need to subset the baits to
# include those taxa that we do want. Given the example, above, we can run:
	cd ${PATH_TO_WD}/probe-design
	phyluce_probe_get_subsets_of_tiled_probes \
		--probes coleoptera-v1-master-probe-list-DUPE-SCREENED.fasta \
		--taxa anogla1 lepdec1 denpon1 \
		--output coleoptera-v1-master-probe-list-DUPE-SCREENED-SUBSET-CLADE_1.fasta
	wait
} # CHECKED

function align_baits_to_genomes {
	cd ${PATH_TO_WD}/probe-design-test
	# Run alignments
	mkdir coleoptera-genome-lastz
	phyluce_probe_run_multiple_lastzs_sqlite --db triCas1+5+strepsiptera-test.sqlite --output coleoptera-genome-lastz --probefile ${PATH_TO_WD}/probe-design/coleoptera-v1-master-probe-list-DUPE-SCREENED.fasta --scaffoldlist agrPla1 anoGla1 denPon1 lepDec1 ontTau1 triCas1 menMol1 --genome-base-path ${PATH_TO_WD}/genomes --identity 50 --cores 24
	wait
	# Extract fasta data
	ln -s ${PATH_TO_WD}/probe-design/coleoptera-genome.conf .
	phyluce_probe_slice_sequence_from_genomes --conf coleoptera-genome.conf --lastz coleoptera-genome-lastz --output coleoptera-genome-fasta --flank 400 --name-pattern "coleoptera-v1-master-probe-list-DUPE-SCREENED.fasta_v_{}.lastz.clean"
	# Match contigs to baits
	mkdir log
	phyluce_assembly_match_contigs_to_probes --contigs coleoptera-genome-fasta --probes ${PATH_TO_WD}/probe-design/coleoptera-v1-master-probe-list-DUPE-SCREENED.fasta --output in-silico-lastz --min-coverage 67 --log-path log
	# Get match counts and extract FASTA information
	# First, extract all of the “good” loci to a monolithic FASTA
	printf "[all]\n" > in-silico-coleoptera-taxon-sets.conf
	for d in ${PATH_TO_WD}/genomes/* ; do printf "`echo ${d} | grep -oE '[a-zA-Z0-9]+$'`\n" >> in-silico-coleoptera-taxon-sets.conf ; done
	mkdir -p taxon-sets/insilico-incomplete ; mkdir -p taxon-sets/insilico-complete
	phyluce_assembly_get_match_counts --locus-db in-silico-lastz/probe.matches.sqlite --taxon-list-config in-silico-coleoptera-taxon-sets.conf --taxon-group 'all' --output taxon-sets/insilico-incomplete/insilico-incomplete.conf --log-path log --incomplete-matrix
	wait
	# Second, extract the FASTA information for each locus into a monolithic FASTA file:
	phyluce_assembly_get_fastas_from_match_counts --contigs ${PATH_TO_WD}/probe-design-test/coleoptera-genome-fasta --locus-db ${PATH_TO_WD}/probe-design-test/in-silico-lastz/probe.matches.sqlite --match-count-output taxon-sets/insilico-incomplete/insilico-incomplete.conf --output insilico-incomplete.fasta --incomplete-matrix insilico-incomplete.incomplete --log-path log
	wait
} # CHECKED

function align_to_conserved_locus {
	cd ${PATH_TO_WD}/probe-design-test/taxon-sets
	mkdir log
	phyluce_align_seqcap_align --fasta ${PATH_TO_WD}/probe-design-test/insilico-incomplete.fasta --output mafft --taxa 7 --incomplete-matrix --cores 24 --no-trim --output-format fasta --log-path log
} # CHECKED

function filter_conserved_locus_alignments {
	cd ${PATH_TO_WD}/probe-design-test/taxon-sets
	phyluce_align_get_gblocks_trimmed_alignments_from_untrimmed --alignments mafft --output mafft-gblocks --b1 0.5 --b4 8 --cores 24 --log log # trim
	wait
	phyluce_align_remove_locus_name_from_nexus_lines --alignments mafft-gblocks --output mafft-gblocks-clean --cores 24 --log-path log # remove locus names
	wait
} # CHECKED

function get_stats {
	cd ${PATH_TO_WD}/probe-design-test/taxon-sets
	phyluce_align_get_align_summary_data --alignments mafft-gblocks-clean --cores 24 --log-path log
} # CHECKED

function generate_incomplete_matrix {
	cd ${PATH_TO_WD}/probe-design-test/taxon-sets
	phyluce_align_get_only_loci_with_min_taxa --alignments mafft-gblocks-clean --taxa 7 --output mafft-gblocks-70p --percent 0.70 --cores 24 --log log
} # CHECKED

function reconcile_best_tree_with_bootreps {
	cd ${PATH_TO_WD}/probe-design-test/taxon-sets
	# Prepare files for raxML
	phyluce_align_format_nexus_files_for_raxml --alignments mafft-gblocks-70p --output mafft-gblocks-70p-raxml --log-path log --charsets
	# Run raxml against this phylip file
	wait
	raxmlHPC-PTHREADS-SSE3 -m GTRGAMMA -N 20 -p 772374015 -n BEST -s mafft-gblocks-70p-raxml/mafft-gblocks-70p.phylip -o menMol1 -T 24
	wait
	raxmlHPC-PTHREADS-SSE3 -m GTRGAMMA -N autoMRE -p 772374015 -b 444353738 -n bootrep -s mafft-gblocks-70p-raxml/mafft-gblocks-70p.phylip -o menMol1 -T 24
	wait
	# Reconcile the best ML tree w/ the bootreps
	raxmlHPC-SSE3 -f b -m GTRGAMMA -t RAxML_bestTree.BEST -z RAxML_bootstrap.bootrep -n FINAL -o menMol1
	wait
	# Rename the tips
	tip_names="[all]\nagrPla1:agrPla1-Agrilus_planipennis-emerald_ash_borer\nanoGla1:anoGla1-Anoplophora_glabripennis-Asian_longhorned_beetle\ndenPon1:denPon1-Dendroctonus_ponderosae-mountain_pine_beetle\nlepDec1:lepDec1-Leptinotarsa_decemlineata-Colorado_potato_beetle\nmenMol1:menMol1-Mengenilla_moldrzyki-Strepsiptera\nontTau1:ontTau1-Onthophagus_taurus-taurus_scarab\ntriCas1:triCas1-Tribolium_castaneum-red_flour_beetle\n"
	printf "${tip_names}" > rename.conf
	phyluce_genetrees_rename_tree_leaves --order left:right --input-format newick --output-format newick --config rename.conf --section all --input RAxML_bipartitions.FINAL --output RAxML_bipartitions.NAME.FINAL.tre
	wait
	prinf "Final tree:  RAxML_bipartitions.NAME.FINAL.tre\n"
}

## SET WORKING DIRECTORY

PATH_TO_WD="phyluce_tutorial_iv/uce-coleoptera"
cd ${PATH_TO_WD}

## EXECUTE FUNCTIONS

printf "> Executing function 'download' [`date`]\n"
download

printf "> Executing function 'extract_sequences' [`date`]\n"
extract_sequences

printf "> Executing function 'mod_headers' [`date`]\n"
mod_headers

printf "> Executing function 'remove_redundant_ncbi' [`date`]\n"
remove_redundant_ncbi

printf "> Executing function 'organize_directories' [`date`]\n"
organize_directories
# ERROR 1: organize_directories: "mv: cannot move 'genomes' to a subdirectory of itself, 'genomes/genomes/genomes'": corrected

printf "> Executing function 'convert_2bit' [`date`]\n"
convert_2bit
# ERROR 2: "faToTwoBit: command not found": corrected (BLAT, ART, and STAMPY had to be installed separately)

printf "> Executing function 'simulate_reads_using_art' [`date`]\n"
simulate_reads_using_art

printf "> Executing function 'merge_read_info' [`date`]\n"
merge_read_info # ERROR 3: "cannot remove 'agrPla1-pe100-reads1.fq': No such file or directory"

printf "> Executing function 'prepare_base_genome' [`date`]\n"
prepare_base_genome # ERROR 4: wrong path to triCas1.fasta

printf "> Executing function 'align_reads_2_genome' [`date`]\n"
align_reads_2_genome

printf "> Executing function 'convert_bams_2_beds' [`date`]\n"
convert_bams_2_beds

printf "> Executing function 'sort_beds' [`date`]\n"
sort_beds

printf "> Executing function 'merge_overlapping_intervals' [`date`]\n"
merge_overlapping_intervals

printf "> Executing function 'remove_repetitive_intervals' [`date`]\n"
remove_repetitive_intervals

printf "> Executing function 'create_bed_config_file' [`date`]\n"
create_bed_config_file # This functions was added to create the configuration file automatically

printf "> Executing function 'find_locus_in_multiple_genomes' [`date`]\n"
find_locus_in_multiple_genomes # ERROR 5: "No section: 'beds'": corrected with new function create_bed_config_file

printf "> Executing function 'find_shared_conserved_loci' [`date`]\n"
find_shared_conserved_loci

printf "> Executing function 'extract_fasta_from_base' [`date`]\n"
extract_fasta_from_base

printf "> Executing function 'design_temporary_bait' [`date`]\n"
design_temporary_bait

printf "> Executing function 'remove_duplicates_from_temporary_bait' [`date`]\n"
remove_duplicates_from_temporary_bait

printf "> Executing function 'align_baits_against_exemplar_genomes' [`date`]\n"
align_baits_against_exemplar_genomes # ERROR 6: automate creation of configuration file and modify phyluce program to skip questions

printf "> Executing function 'find_consistent_loci' [`date`]\n"
find_consistent_loci # ERROR 7: fix path to the required directories

printf "> Executing function 'design_baits_using_all_genomes' [`date`]\n"
design_baits_using_all_genomes # ERROR 8: fix path to the required directories

printf "> Executing function 'remove_duplicates_from_bait_set' [`date`]\n"
remove_duplicates_from_bait_set # ERROR 9: fix path to the required directories

printf "<NOTE: The master probe list that has been filtered of putatively duplicate loci is now located in coleoptera-v1-master-probe-list-DUPE-SCREENED.fasta.>\n"

printf "> Executing function 'subsetting_master_probe_list' [`date`]\n"
subsetting_master_probe_list # ERROR 10: fix path to the required directories

printf "> Start in silico test of the bait design [`date`]\n"
align_baits_to_genomes # This function will align our bait set to the extant genome sequences

align_to_conserved_locus # align the sequence data for each conserved locus in our data set

filter_conserved_locus_alignments # trim the resulting alignments and remove the locus name from the alignments

get_stats # Compute stats across the alignments

generate_incomplete_matrix #  generate a 70% complete matrix

reconcile_best_tree_with_bootreps # Reconcile the best ML tree w/ the bootreps and rename the tips

printf "> All done [`date`]\n"

## Exit
exit
