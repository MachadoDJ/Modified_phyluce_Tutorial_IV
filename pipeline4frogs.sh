#!/bin/bash
set -o errexit

## NOTES
# Tutorial available at http://phyluce.readthedocs.io/en/latest/tutorial-four.html#
# From data download to the creation of a master bait list

# Date: Dec. 08, 2017

## DEPENDENCIES
# Anaconda/ Miniconda
# Phyluce
# SAMtools (should become available after Phyluce installation with CONDA)
# BEDtools (should become available after Phyluce installation with CONDA)
# mod_headers.py (homemade Python script)
# ART (art_illumina v2016.06.05)
# BLAT (Src35)
# BLAT (Src35)
# phyluce_probe_slice_sequence_from_genomes (modified from original, lines 267–273, skip questions)

## DEFINE FUNCTIONS

function convert_2bit {
	cd ${PATH_TO_WD}/genomes
	for file in * ; do faToTwoBit ${file}/${file}.fasta ${file}/${file%.*}.2bit ; done
} # CHECKED

function simulate_reads_using_art {
	cd ${PATH_TO_WD}
	mkdir reads
	cd reads
	art_illumina --paired --in ${PATH_TO_WD}/genomes/nparkeri/nparkeri.fasta       --out nparkeri-pe100-reads    --len 100 --fcov 2 --mflen 200 --sdev 150 -ir 0.0 -ir2 0.0 -dr 0.0 -dr2 0.0 -qs 100 -qs2 100 -na &
	art_illumina --paired --in ${PATH_TO_WD}/genomes/pterribilis/pterribilis.fasta --out pterribilis-pe100-reads --len 100 --fcov 2 --mflen 200 --sdev 150 -ir 0.0 -ir2 0.0 -dr 0.0 -dr2 0.0 -qs 100 -qs2 100 -na &
	art_illumina --paired --in ${PATH_TO_WD}/genomes/sholbrookii/sholbrookii.fasta --out sholbrookii-pe100-reads --len 100 --fcov 2 --mflen 200 --sdev 150 -ir 0.0 -ir2 0.0 -dr 0.0 -dr2 0.0 -qs 100 -qs2 100 -na &
	art_illumina --paired --in ${PATH_TO_WD}/genomes/xlaevis/xlaevis.fasta         --out xlaevis-pe100-reads     --len 100 --fcov 2 --mflen 200 --sdev 150 -ir 0.0 -ir2 0.0 -dr 0.0 -dr2 0.0 -qs 100 -qs2 100 -na &
	art_illumina --paired --in ${PATH_TO_WD}/genomes/xtropicalis/xtropicalis.fasta --out xtropicalis-pe100-reads --len 100 --fcov 2 --mflen 200 --sdev 150 -ir 0.0 -ir2 0.0 -dr 0.0 -dr2 0.0 -qs 100 -qs2 100 -na
	wait
	# NOTE: we’ve dropped menMol1 from the read simulation process. This is largely
	# because it is an outgroup to beetles. We’ll use it later, when we’re performing in
	# silico tests of the UCE bait set.
} # CHECKED

function merge_read_info { # Modified from the original
	cd ${PATH_TO_WD}/reads
	for critter in nparkeri pterribilis sholbrookii xlaevis xtropicalis ; do
		printf "...working on ${critter}\n"
		cat  ${critter}-pe100-reads1.fq ${critter}-pe100-reads2.fq >  ${critter}-pe100-reads.fq
		rm   ${critter}-pe100-reads1.fq ${critter}-pe100-reads2.fq
		gzip ${critter}-pe100-reads.fq
	done
} # CHECKED

function prepare_base_genome {
	cd ${PATH_TO_WD}
	ln -s ${PATH_TO_WD}/genomes/xtropicalis/xtropicalis.fasta .
	mkdir ${PATH_TO_WD}/base
	cd    ${PATH_TO_WD}/base
	cp    ${PATH_TO_WD}/genomes/xtropicalis/xtropicalis.fasta ./
	wait
	stampy.py --species="Xenopus-tropicalis" --assembly="xtropicalis" -G xtropicalis xtropicalis.fasta
	wait
	stampy.py -g xtropicalis -H xtropicalis
	wait
} # CHECKED

function align_reads_2_genome {
	mkdir ${PATH_TO_WD}/alignments
	cd    ${PATH_TO_WD}
	export cores=${NPROCS}
	export base=xtropicalis
	export base_dir=${PATH_TO_WD}/alignments
	for critter in nparkeri pterribilis sholbrookii xlaevis ; do
		export reads=${critter}-pe100-reads.fq.gz
		mkdir -p ${base_dir}/${critter}
		cd ${base_dir}/${critter}
		stampy.py --maxbasequal 93 -g ${PATH_TO_WD}/base/${base} \
			-h ${PATH_TO_WD}/base/${base} --substitutionrate=0.05 -t${cores} \
			--insertsize=400 \
			-M ${PATH_TO_WD}/reads/${reads} | samtools view -Sb - > ${critter}-to-${base}.bam
		wait
	done
	cd ${PATH_TO_WD}/alignments
	mkdir all
	for critter in nparkeri pterribilis sholbrookii xlaevis ; do
		samtools view -h -F 4 -b ${critter}/${critter}-to-xtropicalis.bam > ${critter}/${critter}-to-xtropicalis-MAPPING.bam
		wait
		# rm $critter/$critter-to-xtropicalis.bam
		ln -s ../${critter}/${critter}-to-xtropicalis-MAPPING.bam all/${critter}-to-xtropicalis-MAPPING.bam
		wait
	done
} # CHECKED

function convert_bams_2_beds {
	cd ${PATH_TO_WD}
	mkdir bed
	cd ${PATH_TO_WD}/bed
	for i in ../alignments/all/*.bam ; do
		echo ${i}
		bedtools bamtobed -i ${i} -bed12 > `basename ${i}`.bed
	done
} # CHECKED

function sort_beds {
	cd ${PATH_TO_WD}/bed
	for i in *.bed ; do
		printf "${i}\n"
		bedtools sort -i ${i} > ${i%.*}.sort.bed
	done
} # CHECKED

function merge_overlapping_intervals {
	cd ${PATH_TO_WD}/bed
	for i in *.bam.sort.bed ; do
		printf "${i}\n"
		bedtools merge -i ${i} > ${i%.*}.merge.bed
		wait
	done
	for i in *.bam.sort.merge.bed ; do
		printf "`wc -l ${i}`\n"
	done
} # CHECKED

function remove_repetitive_intervals {
	cd ${PATH_TO_WD}/bed
	for i in *.sort.merge.bed ; do
		phyluce_probe_strip_masked_loci_from_set --bed ${i} \
			--twobit ../genomes/xtropicalis/xtropicalis.2bit \
			--output ${i%.*}.strip.bed --filter-mask 0.25 --min-length 80
	done;
} # CHECKED

function create_bed_config_file {
	cd ${PATH_TO_WD}/bed
	printf "[beds]\n" > bed-files.conf
	for f in *.strip.bed ; do printf "`expr "${f}" : '^\(.[a-zA-Z0-9]*\)'`:${f}\n" >> bed-files.conf ; done
} # CHECKED

function find_locus_in_multiple_genomes {
	cd ${PATH_TO_WD}/bed
	phyluce_probe_get_multi_merge_table --conf bed-files.conf \
		--base-taxon xtropicalis --output anura-to-xtropicalis.sqlite
	wait
} # CHECKED

function find_shared_conserved_loci {
	cd ${PATH_TO_WD}/bed
	phyluce_probe_query_multi_merge_table --db anura-to-xtropicalis.sqlite --base-taxon xtropicalis
	wait
	# Basically, the question boils down to “Should I select only the set of loci shared
	# by all exemplars and the base genome or shoul I be more liberal?”. It’s also a hard
	# question to answer. In most cases, I’m pretty happy selecting n-1 or n-2 where n is
	# the total number of exemplar taxa. In the example below, however, we’ve selected n
	# as the “ideal”. This is largely because we have so little information about
	# anuran genomes - so we want to be pretty darn sure these loci are found in
	# most/all of them.
	phyluce_probe_query_multi_merge_table --db anura-to-xtropicalis.sqlite \
		--base-taxon xtropicalis --output xtropicalis+4.bed --specific-counts 4
} # CHECKED

function extract_fasta_from_base {
	cd ${PATH_TO_WD}/bed
	phyluce_probe_get_genome_sequences_from_bed --bed xtropicalis+4.bed \
		--twobit ../genomes/xtropicalis/xtropicalis.2bit --buffer-to 160 --output xtropicalis+4.fasta
	wait
} # CHECKED

function design_temporary_bait {
	cd ${PATH_TO_WD}/bed
	phyluce_probe_get_tiled_probes \
	--input xtropicalis+4.fasta \
	--probe-prefix "uce-" \
	--design anura-v1 \
	--designer faircloth \
	--tiling-density 3 \
	--two-probes \
	--overlap middle \
	--masking 0.25 \
	--remove-gc \
	--output xtropicalis+4.temp.probes
} # CHECKED

function remove_duplicates_from_temporary_bait {
	cd ${PATH_TO_WD}/bed
	phyluce_probe_easy_lastz \
	--target xtropicalis+4.temp.probes --query xtropicalis+4.temp.probes \
	--identity 50 --coverage 50 \
	--output xtropicalis+4.temp.probes-TO-SELF-PROBES.lastz
	wait
	phyluce_probe_remove_duplicate_hits_from_probes_using_lastz \
	--fasta xtropicalis+4.temp.probes \
	--lastz xtropicalis+4.temp.probes-TO-SELF-PROBES.lastz \
	--probe-prefix=uce-
	wait
} # CHECKED

function align_baits_against_exemplar_genomes {
	cd ${PATH_TO_WD}
	mkdir probe-design
	cd ${PATH_TO_WD}/probe-design # Modified from original - set the correct working directory
	mkdir anura-genome-lastz # Modified from original: Create an empty directory required for the next step
	phyluce_probe_run_multiple_lastzs_sqlite \
		--probefile ${PATH_TO_WD}/bed/xtropicalis+4.temp-DUPE-SCREENED.probes \
		--scaffoldlist nparkeri pterribilis sholbrookii xlaevis xtropicalis anolis \
		--genome-base-path ${PATH_TO_WD}/genomes \
		--identity 50 \
		--cores ${NPROCS} \
		--db xtropicalis+4+anolis.sqlite \
		--output anura-genome-lastz
	wait
	# This next few ines were added to the orignal pipeline to automatically create the
	# required configuration file
	printf "[scaffolds]\n" > anura-genome.conf
	for f in ${PATH_TO_WD}/genomes/*/*.2bit ; do printf "`echo ${f%.2bit} | grep -oE '[^/]+$'`:${f}\n" >> anura-genome.conf ; done
	# Here buffering each locus to 180 bp to give us a little more room to work with
	# during the probe design step.
	phyluce_probe_slice_sequence_from_genomes \
		--conf anura-genome.conf \
		--lastz anura-genome-lastz \
		--probes 180 \
		--name-pattern "xtropicalis+4.temp-DUPE-SCREENED.probes_v_{}.lastz.clean" \
		--output anura-genome-fasta
	# You might encounter a problem here, since phyluce_probe_slice_sequence_from_genomes
	# might stop and ask you about creating/ overwriting the anura-genome-fasta
	# directory (it seems it asks you if you want to overwrite it even if there was
	# originally no directory there!). I edited phyluce_probe_slice_sequence_from_genomes
	# to avoid this problem.
	wait
} # CHECKED

function find_consistent_loci {
	cd ${PATH_TO_WD}/probe-design/anura-genome-lastz
	phyluce_probe_get_multi_fasta_table \
		--fastas ../anura-genome-fasta \
		--output multifastas.sqlite \
		--base-taxon xtropicalis
	wait
	phyluce_probe_query_multi_fasta_table \
		--db multifastas.sqlite \
		--base-taxon xtropicalis
	wait
	phyluce_probe_query_multi_fasta_table \
		--db multifastas.sqlite \
		--base-taxon xtropicalis \
		--output xtropicalis+4-back-to-4.conf \
		--specific-counts 4
	wait
	phyluce_probe_query_multi_fasta_table \
		--db multifastas.sqlite \
		--base-taxon xtropicalis
	wait
	phyluce_probe_query_multi_fasta_table \
		--db multifastas.sqlite \
		--base-taxon xtropicalis \
		--output xtropicalis+4-back-to-4.conf \
		--specific-counts 4
	wait
} # CHECKED

function design_baits_using_all_genomes {
	cd ${PATH_TO_WD}/probe-design/anura-genome-lastz
	phyluce_probe_get_tiled_probe_from_multiple_inputs \
		--fastas ../anura-genome-fasta \
		--multi-fasta-output xtropicalis+4-back-to-4.conf \
		--probe-prefix "uce-" \
		--designer faircloth \
		--design anura-v1 \
		--tiling-density 3 \
		--overlap middle \
		--masking 0.25 \
		--remove-gc \
		--two-probes \
		--output anura-v1-master-probe-list.fasta
	wait
	# Note that the number of baits that we’ve designed to target ~1200 conserved loci is
	# quite high - this is because we’re including roughly 2 baits for ~1200 loci across 7
	# exemplar taxa (16926 is the theoretical maximum).
} # CHECKED

function remove_duplicates_from_bait_set {
	cd ${PATH_TO_WD}/probe-design/anura-genome-lastz
	phyluce_probe_easy_lastz \
		--target anura-v1-master-probe-list.fasta \
		--query anura-v1-master-probe-list.fasta \
		--identity 50 \
		--coverage 50 \
		--output anura-v1-master-probe-list-TO-SELF-PROBES.lastz
	wait
	phyluce_probe_remove_duplicate_hits_from_probes_using_lastz \
		--fasta anura-v1-master-probe-list.fasta \
		--lastz anura-v1-master-probe-list-TO-SELF-PROBES.lastz \
		--probe-prefix=uce-
	wait
# The master probe list that has been filtered of putatively duplicate loci is now located
# in anura-v1-master-probe-list-DUPE-SCREENED.fasta.
} # CHECKED

function subsetting_master_probe_list {
# Sometimes we might not want to synthesize all of the baits for all of the loci. For
# instance, we might be enriching loci from species that are nested within the clade
# defined by ((‘Anoplophora glabripennis (Asian longhorned beetle)’:’Leptinotarsa
# decemlineata (Colorado potato beetle)’)’Dendroctonus ponderosae (mountain pine
# beetle)’), and because we’re only working with these species, we might want to drop the
# baits targeting UCE loci in Agrilus planipennis (emerald ash borer), Tribolium castaneum
# (red flour beetle), Onthophagus taurus (taurus scarab), and Mengenilla moldrzyki
# (Strepsiptera). This is actually pretty easy to do - we just need to subset the baits to
# include those taxa that we do want. Given the example, above, we can run:
	cd ${PATH_TO_WD}/probe-design/anura-genome-lastz
	phyluce_probe_get_subsets_of_tiled_probes \
		--probes anura-v1-master-probe-list-DUPE-SCREENED.fasta \
		--taxa nparkeri pterribilis  sholbrookii  xlaevis  xtropicalis \
		--output anura-v1-master-probe-list-DUPE-SCREENED-SUBSET-CLADE_1.fasta
	wait
} # CHECKED

function align_baits_to_genomes {
	cd ${PATH_TO_WD}/probe-design-test
	# Run alignments
	mkdir anura-genome-lastz
	phyluce_probe_run_multiple_lastzs_sqlite --db xtropicalis+4+anolis-test.sqlite --output anura-genome-lastz --probefile ${PATH_TO_WD}/probe-design/anura-genome-lastz/anura-v1-master-probe-list-DUPE-SCREENED.fasta --scaffoldlist nparkeri pterribilis sholbrookii xlaevis xtropicalis anolis --genome-base-path ${PATH_TO_WD}/genomes --identity 50 --cores ${NPROCS}
	wait
	# Extract fasta data
	ln -s ${PATH_TO_WD}/probe-design/anura-genome.conf .
	phyluce_probe_slice_sequence_from_genomes --conf anura-genome.conf --lastz anura-genome-lastz --output anura-genome-fasta --flank 400 --name-pattern "anura-v1-master-probe-list-DUPE-SCREENED.fasta_v_{}.lastz.clean"
	# Match contigs to baits
	mkdir log
	phyluce_assembly_match_contigs_to_probes --contigs anura-genome-fasta --probes ${PATH_TO_WD}/probe-design/anura-genome-lastz/anura-v1-master-probe-list-DUPE-SCREENED.fasta --output in-silico-lastz --min-coverage 67 --log-path log
	# Get match counts and extract FASTA information
	# First, extract all of the “good” loci to a monolithic FASTA
	printf "[all]\n" > in-silico-anura-taxon-sets.conf
	for d in ${PATH_TO_WD}/genomes/* ; do printf "`echo ${d} | grep -oE '[a-zA-Z0-9]+$'`\n" >> in-silico-anura-taxon-sets.conf ; done
	mkdir -p taxon-sets/insilico-incomplete ; mkdir -p taxon-sets/insilico-complete
	phyluce_assembly_get_match_counts --locus-db in-silico-lastz/probe.matches.sqlite --taxon-list-config in-silico-anura-taxon-sets.conf --taxon-group 'all' --output taxon-sets/insilico-incomplete/insilico-incomplete.conf --log-path log --incomplete-matrix
	# Second, extract the FASTA information for each locus into a monolithic FASTA file:
	phyluce_assembly_get_fastas_from_match_counts --contigs ${PATH_TO_WD}/probe-design-test/anura-genome-fasta --locus-db ${PATH_TO_WD}/probe-design-test/in-silico-lastz/probe.matches.sqlite --match-count-output taxon-sets/insilico-incomplete/insilico-incomplete.conf --output insilico-incomplete.fasta --incomplete-matrix insilico-incomplete.incomplete --log-path log
	wait
} # CHECKED

function align_to_conserved_locus {
	cd ${PATH_TO_WD}/probe-design-test/taxon-sets
	mkdir log
	phyluce_align_seqcap_align --fasta ${PATH_TO_WD}/probe-design-test/insilico-incomplete.fasta --output mafft --taxa 7 --incomplete-matrix --cores ${NPROCS} --no-trim --output-format fasta --log-path log
} # CHECKED

function filter_conserved_locus_alignments {
	cd ${PATH_TO_WD}/probe-design-test/taxon-sets
	phyluce_align_get_gblocks_trimmed_alignments_from_untrimmed --alignments mafft --output mafft-gblocks --b1 0.5 --b4 8 --cores ${NPROCS} --log log # trim
	wait
	phyluce_align_remove_locus_name_from_nexus_lines --alignments mafft-gblocks --output mafft-gblocks-clean --cores ${NPROCS} --log-path log # remove locus names
	wait
} # CHECKED

function get_stats {
	cd ${PATH_TO_WD}/probe-design-test/taxon-sets
	phyluce_align_get_align_summary_data --alignments mafft-gblocks-clean --cores ${NPROCS} --log-path log
} # CHECKED

function generate_incomplete_matrix {
	cd ${PATH_TO_WD}/probe-design-test/taxon-sets
	phyluce_align_get_only_loci_with_min_taxa --alignments mafft-gblocks-clean --taxa 6 --output mafft-gblocks-70p --percent 0.70 --cores ${NPROCS} --log log
} # CHECKED

function reconcile_best_tree_with_bootreps {
	cd ${PATH_TO_WD}/probe-design-test/taxon-sets
	# Prepare files for raxML
	phyluce_align_format_nexus_files_for_raxml --alignments mafft-gblocks-70p --output mafft-gblocks-70p-raxml --log-path log --charsets
	# Run raxml against this phylip file
	wait
	raxmlHPC-PTHREADS-SSE3 -m GTRGAMMA -N 20 -p 772374015 -n BEST -s mafft-gblocks-70p-raxml/mafft-gblocks-70p.phylip -o anolis -T ${NPROCS}
	wait
	raxmlHPC-PTHREADS-SSE3 -m GTRGAMMA -N autoMRE -p 772374015 -b 444353738 -n bootrep -s mafft-gblocks-70p-raxml/mafft-gblocks-70p.phylip -o anolis -T ${NPROCS}
	wait
	# Reconcile the best ML tree w/ the bootreps
	raxmlHPC-SSE3 -f b -m GTRGAMMA -t RAxML_bestTree.BEST -z RAxML_bootstrap.bootrep -n FINAL -o anolis
	wait
	# Rename the tips
	tip_names="[all]\nnparkeri:nparkeri-Nanorana_parkeri\npterribilis:pterribilis-Phyllobates_terribilis\nsholbrookii:sholbrookii-Scaphiopus_holbrookii\nxlaevis:xlaevis-Xenopus_tropicalis\nxtropicalis:xtropicalis-Xenopus_tropicalis\nanolis:anolis-Anolis_carolinensis\n"
	printf "${tip_names}" > rename.conf
	phyluce_genetrees_rename_tree_leaves --order left:right --input-format newick --output-format newick --config rename.conf --section all --input RAxML_bipartitions.FINAL --output RAxML_bipartitions.NAME.FINAL.tre
	wait
	prinf "Final tree: RAxML_bipartitions.NAME.FINAL.tre\n"
}

## SET VARIABLES AND WD

export NPROCS=24 # Change this line to match the number of cpus available in your computer
export PATH_TO_WD="~/projects/171208_phyluce"  # Change this line to insert the whole path to your working directory
cd ${PATH_TO_WD}

## EXECUTE FUNCTIONS

printf "> Executing function 'convert_2bit' [`date`]\n"
convert_2bit

printf "> Executing function 'simulate_reads_using_art' [`date`]\n"
simulate_reads_using_art

printf "> Executing function 'merge_read_info' [`date`]\n"
merge_read_info

printf "> Executing function 'prepare_base_genome' [`date`]\n"
prepare_base_genome

printf "> Executing function 'align_reads_2_genome' [`date`]\n"
align_reads_2_genome

printf "> Executing function 'convert_bams_2_beds' [`date`]\n"
convert_bams_2_beds

printf "> Executing function 'sort_beds' [`date`]\n"
sort_beds

printf "> Executing function 'merge_overlapping_intervals' [`date`]\n"
merge_overlapping_intervals

printf "> Executing function 'remove_repetitive_intervals' [`date`]\n"
remove_repetitive_intervals

printf "> Executing function 'create_bed_config_file' [`date`]\n"
create_bed_config_file

printf "> Executing function 'find_locus_in_multiple_genomes' [`date`]\n"
find_locus_in_multiple_genomes

printf "> Executing function 'find_shared_conserved_loci' [`date`]\n"
find_shared_conserved_loci

printf "> Executing function 'extract_fasta_from_base' [`date`]\n"
extract_fasta_from_base

printf "> Executing function 'design_temporary_bait' [`date`]\n"
design_temporary_bait

printf "> Executing function 'remove_duplicates_from_temporary_bait' [`date`]\n"
remove_duplicates_from_temporary_bait

printf "> Executing function 'align_baits_against_exemplar_genomes' [`date`]\n"
align_baits_against_exemplar_genomes

printf "> Executing function 'find_consistent_loci' [`date`]\n"
find_consistent_loci

printf "> Executing function 'design_baits_using_all_genomes' [`date`]\n"
design_baits_using_all_genomes

printf "> Executing function 'remove_duplicates_from_bait_set' [`date`]\n"
remove_duplicates_from_bait_set

printf "<NOTE: The master probe list that has been filtered of putatively duplicate loci is now located in anura-v1-master-probe-list-DUPE-SCREENED.fasta.>\n"

printf "> Executing function 'subsetting_master_probe_list' [`date`]\n"
subsetting_master_probe_list

printf "> Start in silico test of the bait design [`date`]\n"
align_baits_to_genomes # This function will align our bait set to the extant genome sequences

align_to_conserved_locus # align the sequence data for each conserved locus in our data set

filter_conserved_locus_alignments # trim the resulting alignments and remove the locus name from the alignments

get_stats # Compute stats across the alignments

generate_incomplete_matrix #  generate a 70% complete matrix

reconcile_best_tree_with_bootreps # Reconcile the best ML tree w/ the bootreps and rename the tips

printf "> All done [`date`]\n"

## Exit
exit
